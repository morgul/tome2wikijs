# Tome 2 Wiki.js

This project is a simple (in concept) converter to generate a wiki.js version of a Tome wiki. Since I'm the author of 
Tome, I doubt this project will be useful to anyone else but me, but that also means it's done when I say it is, which 
is nice. So, the rest of this is going to be me talking to me in the future who needs to remember how this all worked.

## Conversion Process Theory

Conceptually, the conversion process is as simple as this:

1. Launch up an instance of Wiki.js and configure it correctly
2. Read in the tome 'database' (just JSON files)
3. Use the GraphQL API to add the users, permissions, and pages and revisions in to the Wiki.JS instance.
4. ???
5. Profit

The problem comes in with the GraphQL API. Not only does it involve learning how to make calls with the API (which 
isn't quite completely undocumented), but it doesn't support something we need for a conversion, like the ability to 
add pages with a given timestamp on them. It would be very nice if the timestamps were right... or, at least, not 7 
years out of date). So, in order to add those kinds of things, direct pg access might be required.

(Note: Tome was really awful about created/updated dates. Several wikis have none, while others have some, but not 
everything does.)

## Conversion Process Practice

So, the actual conversion steps are going to be:

1. Launch an instance of Wiki.js locally (via docker) and get it configured properly.
   1. Set the administrator email and password.
   2. Set the name, description, etc.
   3. Enable API Access, and get the token for API requests
   4. Enable Google Authentication, named `'Google'`.
2. Load all the Tome JSON files
3. Create Users
   1. List all Providers, and look for the 'Google' one.
   2. Add Users (just email and name)
4. Delete any existing pages (dev only)
5. Create Pages
   1. List all pages and group revisions
   2. For each page in the list, call create for the first revision, and update for the rest.
   3. Hit the db to update the author and date, if applicable.
   4. Rebuild the page tree
   5. Rerender the pages
6. Upload assets
   1. This will need to be a manual process, since they don't have an API for uploading the files.

### Launch a Wiki.js instance

Make sure you are in the root of this project, and then run the following command:

* `docker-compose -f compose/wiki.yml -p tome2wikijs up -d`

This will launch a wiki.js instance, along with the database. (This is important, as what we'll be copying to the 
remote server is the database directory, eventually.)

Now, configure it however you want it configured. (Site name, admin user, etc, etc.)

#### API Access

You will need to turn on API access, and generate an API token, like below:

![](images/APIKey.png)

#### Ensure Google Auth is Configured

You will also need to turn on the Google Provider for authentication, since that's what Tome used. You **must** name it 
'Google'. (If you fail to do so, the user conversion will fail, since we have to look up the provider's key by 
something.)

![](images/GoogleAuth.png)



