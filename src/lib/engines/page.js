// ---------------------------------------------------------------------------------------------------------------------
// Page Engine
// ---------------------------------------------------------------------------------------------------------------------

export function fixWikiLinks(content = '')
{
    // const linkRegEx = /\[([^\]]+)\]\(([^\)]+)\)/g;
    const linkRegEx = /\[([^\]]+)\]\((.+?)\)(?!\))/g;
    return content
        .replaceAll(linkRegEx, (match, linkTitle, linkUrl) =>
        {
            if(linkUrl[0] !== 'h' && linkUrl[0] !== '/')
            {
                return `[${ linkTitle }](/${ linkUrl })`;
            }
            else
            {
                return match;
            }
        })
        .replaceAll(/^\[(.*)\]: (.*)$/gm, (match, linkTitle, linkUrl) =>
        {
            if(linkUrl[0] !== 'h' && linkUrl[0] !== '/')
            {
                return `[${ linkTitle }]: /${ linkUrl }`;
            }
            else
            {
                return match;
            }
        })
        .replaceAll('pull-right', 'float-right')
        .replaceAll('pull-left', 'float-left')
        .replaceAll('<proficiency></proficiency>', '<span class="proficiency"></span>')
        .replaceAll('<ability></ability>', '<span class="ability"></span>')
        .replaceAll('<boost></boost>', '<span class="boost"></span>')
        .replaceAll('<challenge></challenge>', '<span class="challenge"></span>')
        .replaceAll('<difficulty></difficulty>', '<span class="difficulty"></span>')
        .replaceAll('<setback></setback>', '<span class="setback"></span>')
        .replaceAll('<force></force>', '<span class="force"></span>')
        .replaceAll('<success></success>', '<span class="success"></span>')
        .replaceAll('<failure></failure>', '<span class="failure"></span>')
        .replaceAll('<advantage></advantage>', '<span class="advantage"></span>')
        .replaceAll('<threat></threat>', '<span class="threat"></span>')
        .replaceAll('<triumph></triumph>', '<span class="triumph"></span>')
        .replaceAll('<despair></despair>', '<span class="despair"></span>')
        .replaceAll('<light-side></light-side>', '<span class="light-side"></span>')
        .replaceAll('<dark-side></dark-side>', '<span class="dark-side"></span>')
        .replaceAll('<force-point></force-point>', '<span class="force-point"></span>')
}

export function escapeString(content = '')
{
    return JSON.stringify(content);
}

export function cleanPageBody(content = '')
{
    return escapeString(fixWikiLinks(content));
}

// ---------------------------------------------------------------------------------------------------------------------
