// ---------------------------------------------------------------------------------------------------------------------
// Conversion Manager
// ---------------------------------------------------------------------------------------------------------------------

// Resource Access
import tomeRA from '../resource-access/tome.js';
import wikiRA from '../resource-access/wiki.js';
import wikiPgRA from '../resource-access/wikiPg.js';

// ---------------------------------------------------------------------------------------------------------------------

class ConvertManager
{
    async clearPages()
    {
        console.log('Listing all existing pages...');

        const pages = await wikiRA.listPages();

        console.log('  ...done');

        console.log(`Deleting ${ pages.length } pages...`);
        return Promise.all(pages.map(async (page) =>
        {
            return wikiRA.deletePage(page.id);
        }));
    }

    async reRenderPages()
    {
        console.log('Listing all existing pages...');

        const pages = await wikiRA.listPages();

        console.log('  ...done');

        console.log(`Rerendering ${ pages.length } pages...`);
        return Promise.all(pages.map(async (page) =>
        {
            return wikiRA.renderPage(page.id);
        }));
    }

    async convertUsers()
    {
        const tomeUsers = tomeRA.listUsers();

        console.log(`Converting ${ tomeUsers.length } users...`)

        // List Providers
        const providers = await wikiRA.listProviders();
        const googleProvider = providers.find((provider) => provider.displayName === 'Google')

        // Add all the wiki users
        for(const user of tomeUsers)
        {
            console.log(`  Adding user: ${ user.email }...`);
            const { users: { create: { responseResult } } }
                = await wikiRA.addUser(user.email, user.displayName, googleProvider.key);
            console.log(`    ...${ responseResult.succeeded ? 'done' : 'failed! Reason: ' + responseResult.message }`);
        }

        console.log('Converting Users done.')
    }

    async convertPages()
    {
        const pages = tomeRA.listPages();

        console.log(`Converting ${ pages.length } pages...`);

        for(const page of pages)
        {
            let pageID;
            console.log(` Creating Page '${ page.revisions[0].title }' at '${ page.path }' with ${ page.revisions.length } revisions...`);

            // We need to know the index in order to control the logic correctly for create vs update, hence old school
            for(let idx = 0; idx < page.revisions.length; idx++)
            {
                const rev = page.revisions[idx];
                const pageUrl = rev.url === 'welcome' ? 'home' : rev.url;
                const author = await wikiPgRA.getUser(rev.userID);

                if(idx === 0)
                {
                    console.log('    Adding page...');
                    const results = await wikiRA.createPage(rev.title, pageUrl, rev.body, '', rev.tags);
                    if(results.responseResult.succeeded)
                    {
                        pageID = results.page.id;

                        await wikiPgRA.updatePageAuthor(pageID, author.id, rev.created);
                        await wikiPgRA.updatePageCreator(pageID, author.id, page.created);
                    }
                    else
                    {
                        console.log('    ...failed. Message:', results.responseResult.message);
                    }
                }
                else
                {
                    console.log(`    Adding revision '${ rev.id }'...`);

                    if(pageID)
                    {
                        await wikiRA.updatePage(pageID, rev.title, pageUrl, rev.body, '', rev.tags);
                        await wikiPgRA.updatePageAuthor(pageID, author.id, rev.created);
                    }
                    else
                    {
                        console.warn('      Skipping revision.')
                    }
                }
            }
        }

        // Rerender Pages
        this.reRenderPages();

        console.log('Rebuilding Page Tree...');
        await wikiRA.rebuildPageTree();
        console.log('  ...done.');
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new ConvertManager();

// ---------------------------------------------------------------------------------------------------------------------
