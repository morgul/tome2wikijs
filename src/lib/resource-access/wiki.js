// ---------------------------------------------------------------------------------------------------------------------
// Wiki.js GraphQL Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { GraphQLClient, gql } from 'graphql-request'
import { cleanPageBody, escapeString } from '../engines/page.js';

// ---------------------------------------------------------------------------------------------------------------------

class WikiJSResourceAccess
{
    #apiEndpoint;
    #apiToken;
    #client;

    async init(apiEndpoint, apiToken)
    {
        this.#apiEndpoint = apiEndpoint;
        this.#apiToken = apiToken;
        this.#client = new GraphQLClient(this.#apiEndpoint, {
            headers: {
                authorization: `Bearer ${ this.#apiToken }`
            }
        });
    }

    // -----------------------------------------------------------------------------------------------------------------

    async _request(query)
    {
        return this.#client.request(query);
    }

    // -----------------------------------------------------------------------------------------------------------------

    async listPages()
    {
        const query = gql`query pages {
            pages {
              list { id, title, description, path }
            }
        }`;

        const { pages: { list } } = await this._request(query);

        return list;
    }

    async listProviders()
    {
        const query = gql`query authentication {
            authentication {
                activeStrategies {
                    key, displayName
                }
            }
        }`;

        const { authentication: { activeStrategies } } = await this._request(query);

        return activeStrategies;
    }

    async addUser(email, name, providerKey)
    {
        if(!name)
        {
            name = email.split('@')[0].trim();
        }

        const query = gql`mutation users {
            users {
                create(
                    email: "${ email }"
                    name: "${ name }"
                    providerKey: "${ providerKey }"
                    groups: []
                    mustChangePassword: false
                    sendWelcomeEmail: false
                ) { responseResult { message, succeeded } }
            }
        }`;

        return this._request(query);
    }

    async createPage(title, path, content = '_empty_', description = '', tags = [])
    {
        content = cleanPageBody(content);
        description = escapeString(description);

        const query = gql`mutation pages {
            pages {
                create(
                    content: ${ content }
                    description: ${ description }
                    editor: "markdown"
                    isPublished: true
                    isPrivate: false
                    locale: "en"
                    path: "${ path }"
                    tags: [ ${ tags.map((t) => `"${ t }"`).join(',') } ]
                    title: "${ title }"
                )
                {
                    responseResult { message, succeeded },
                    page { id }
                }
            }
        }`;

        const { pages: { create } } = await this._request(query);

        return create;
    }

    async updatePage(id, title, path, content = '', description = '', tags = [])
    {
        content = cleanPageBody(content);
        description = escapeString(description);

        const query = gql`mutation pages {
            pages {
                update(
                    id: ${ id }
                    content: ${ content }
                    description: ${ description }
                    editor: "markdown"
                    isPublished: true
                    isPrivate: false
                    locale: "en"
                    path: "${ path }"
                    tags: [ ${ tags.map((t) => `"${ t }"`).join(',') } ]
                    title: "${ title }"
                )
                {
                    responseResult { message, succeeded }
                }
            }
        }`;

        const { pages: { update: { responseResult } } } = await this._request(query);

        return responseResult;
    }

    async deletePage(pageID)
    {
        const query = gql`
        mutation pages {
            pages {
                delete(id: ${ pageID })
                {
                    responseResult { message, succeeded }
                }
            }
        }`

        const { pages: { delete: { responseResult } } } = await this._request(query);

        return responseResult;
    }

    async renderPage(pageID)
    {
        const query = gql`
        mutation pages {
            pages {
                render(id: ${ pageID })
                {
                    responseResult { message, succeeded }
                }
            }
        }`

        const { pages: { render: { responseResult } } } = await this._request(query);

        return responseResult;
    }

    async rebuildPageTree()
    {
        const query = gql`
        mutation pages {
            pages {
                rebuildTree
                {
                    responseResult { message, succeeded }
                }
            }
        }`

        const { pages: { rebuildTree: { responseResult } } } = await this._request(query);

        return responseResult;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new WikiJSResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
