// ---------------------------------------------------------------------------------------------------------------------
// Wiki Postgres
// ---------------------------------------------------------------------------------------------------------------------

import knex from 'knex';

// ---------------------------------------------------------------------------------------------------------------------

class WikiPGResourceAccess
{
    #db;

    constructor()
    {
        this.#db = new knex({
            client: 'pg',
            connection: {
                host : '127.0.0.1',
                port : 5432,
                user : 'gneadmin',
                password : '##UnspokenSkiesTripodCopilotRebuilt7##',
                database : 'wiki'
            }
        });
    }

    // -----------------------------------------------------------------------------------------------------------------

    async getUser(email)
    {
        return (await this.#db
            .select('id', 'email', 'name', 'providerKey')
            .from('users')
            .where({ email }))[0];
    }

    async updatePageAuthor(pageId, authorId, updatedAt)
    {
        const updateParams = { authorId };

        if(updatedAt)
        {
            updateParams.updatedAt = updatedAt;
        }

        // Update the main page record
        await this.#db
            .update(updateParams)
            .from('pages')
            .where({ id: pageId });
    }

    async updatePageCreator(pageId, creatorId, createdAt)
    {
        const updateParams = { creatorId };

        if(createdAt)
        {
            updateParams.createdAt = createdAt;
        }

        // Update the main page record
        await this.#db
            .update(updateParams)
            .from('pages')
            .where({ id: pageId });
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new WikiPGResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
