// ---------------------------------------------------------------------------------------------------------------------
// Tome Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'path';
import { readFile } from 'fs/promises';

// ---------------------------------------------------------------------------------------------------------------------

class TomeDBResourceAccess
{
    #usersDB = new Map();
    #pagesDB = new Map();
    #revDB = new Map();
    #commentsDB = new Map();

    async init(dbPath)
    {
        const usersDBStr = await readFile(resolve(dbPath, 'users.json'), { encoding: 'utf8' })
            .catch(() =>
            {
                console.warn('Failed to open:', resolve(dbPath, 'users.json'))
                return '{}'
            });

        const pagesDBStr = await readFile(resolve(dbPath, 'pages.json'), { encoding: 'utf8' })
            .catch(() =>
            {
                console.warn('Failed to open:', resolve(dbPath, 'pages.json'))
                return '{}'
            });

        const revDBStr = await readFile(resolve(dbPath, 'revisions.json'), { encoding: 'utf8' })
            .catch(() =>
            {
                console.warn('Failed to open:', resolve(dbPath, 'revisions.json'))
                return '{}'
            });

        const commentsDBStr = await readFile(resolve(dbPath, 'comments.json'), { encoding: 'utf8' })
            .catch(() =>
            {
                console.warn('Failed to open:', resolve(dbPath, 'comments.json'))
                return '{}'
            });

        // Import Users DB
        try
        {
            const usersDBObj = JSON.parse(usersDBStr);
            this.#usersDB = new Map(Object.entries(usersDBObj));
        }
        catch(error)
        {
            console.error('Failed to parse JSON:', error.stack);
        }

        // Import Pages DB
        try
        {
            const pagesDBObj = JSON.parse(pagesDBStr);
            this.#pagesDB = new Map(Object.entries(pagesDBObj));
        }
        catch(error)
        {
            console.error('Failed to parse JSON:', error.stack);
        }

        // Import Revisions DB
        try
        {
            const revDBObj = JSON.parse(revDBStr);
            this.#revDB = new Map(Object.entries(revDBObj));
        }
        catch(error)
        {
            console.error('Failed to parse JSON:', error.stack);
        }

        // Import Comments DB
        try
        {
            const commentsDBObj = JSON.parse(commentsDBStr);
            this.#commentsDB = new Map(Object.entries(commentsDBObj));
        }
        catch(error)
        {
            console.error('Failed to parse JSON:', error.stack);
        }
    }

    listUsers()
    {
        return Array.from(this.#usersDB.values());
    }

    listPages()
    {
        return Array.from(this.#pagesDB.values())
            .map((page) =>
            {
                return {
                    tomeID: page.id,
                    path: page.url,
                    created: (new Date(page.created)).toJSON(),
                    revisions: this.listRevisions(page.id)
                        .map((rev) =>
                        {
                            return {
                                ...rev,
                                created: (new Date(rev.created)).toJSON()
                            }
                        })
                };
            });
    }

    listRevisions(pageID)
    {
        const revisions = Array.from(this.#revDB.values());

        if(pageID)
        {
            return revisions.filter((rev) => rev.pageID === pageID);
        }
        else
        {
            return revisions;
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new TomeDBResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
