// ---------------------------------------------------------------------------------------------------------------------
// Tome to WikiJS Conversion
// ---------------------------------------------------------------------------------------------------------------------

// Managers
import convertMan from './lib/manager/convert.js';

// Resource Access
import tomeRA from './lib/resource-access/tome.js';
import wikiRA from './lib/resource-access/wiki.js';

// ---------------------------------------------------------------------------------------------------------------------

//TODO: These should be passed in, not hard-coded!
const tomeDBPath = '../sa-studios/rfiuniverse.com/db/';
const wikiAPIEndpoint = 'http://localhost:3000/graphql';
const wikiAPIToken = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGkiOjEsImdycCI6MSwiaWF0IjoxNjQ4NDI2NzUwLCJleHAiOjE2Nzk5ODQzNTAsImF1ZCI6InVybjp3aWtpLmpzIiwiaXNzIjoidXJuOndpa2kuanMifQ.TpmLpdB0CZ5QpMEZtikz1wET24JpB2anV-ZtSks0WDNu045724jSgg7ayKPplPPD9jVmLixjeLk8gKnRMYFO63nupBoc7CBW-sA3Djepv-R69mgk-xBEmztZa5EWKrAqAJWRtD95Rj6o2WTwkysHPH-1yAtgxeHLZDrhqotHoX_CQnXklkE9h1Xa4N2rBG3RQnQfC7DKsYKjnAzRnQR5d5MoXLpazwhH_6G9K8Yy2jgSI-ggMXH8UoAatFz6OdfDIo7lS1Oz_BEHvzN2dkes2MJjqXDj0dUj7MLcuka_9or5Cu7XbuRxGcHh9rq69Voo9XDEXSlBXuClnt10zxS2Fw';

// ---------------------------------------------------------------------------------------------------------------------

console.log('Initializing...');

await tomeRA.init(tomeDBPath);
await wikiRA.init(wikiAPIEndpoint, wikiAPIToken);

console.log('Initialization Complete.\n')

// ---------------------------------------------------------------------------------------------------------------------

console.log('Conversion Starting...')

await convertMan.clearPages();

await convertMan.convertUsers();
await convertMan.convertPages();

console.log('Conversion Complete.')

// ---------------------------------------------------------------------------------------------------------------------

process.exit(0);

// ---------------------------------------------------------------------------------------------------------------------
